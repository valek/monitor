package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

type ConfPwd struct {
	Template string // Файл шаблона
	PwdHtml  string // Куда ложить готовые html
	PwdImg   string // Папка где лежать картинки
}

func main() {
	check_loop()

}

func chek_status() {
	fmt.Println(">>> Работаем <<<")
	runCommand("net", "use", "x:", "/delete")
	// Парсим json файл
	file_json, err := os.Open("conf.json")
	if err != nil {
		fmt.Printf("Не возможно найти файл настроки: %v\n", err)
		os.Exit(1)
	}
	decoderConf := json.NewDecoder(file_json)
	resj := ConfPwd{}
	err = decoderConf.Decode(&resj)
	if err != nil {
		fmt.Println("error:", err)
	}
	// Загружаем файл с шаблоном
	input, err := ioutil.ReadFile(resj.Template)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Создаем массив для записи в него имен картинок
	filelist := make(map[int]string)
	files, _ := ioutil.ReadDir(resj.PwdImg)
	//fmt.Println(resj.PwdImg)
	//Пробегаемся по нему и пишим а filelist[]
	for j, f := range files {
		//Записали имя файла в массив
		filelist[j] = f.Name()
		if filelist[0] != "" {
			fmt.Println("Опа!! Файло!")
			if err := runCommand("net", "use", "x:", `\\saikov\ТМ Здоровье\Монитор вызова\img\img`, "/persistent:yes"); err != nil {
				// если жиск подключен, то отключаем его и цепляем заного
				runCommand("net", "use", "x:", "/delete")
				runCommand("net", "use", "x:", `\\saikov\ТМ Здоровье\Монитор вызова\img\img`, "/persistent:yes")
				fmt.Println(">>> Подключили диск <<<")
				//Заменяем слово "zamena" на имя файла картинки
				output := bytes.Replace(input, []byte("zamena"), []byte(filelist[j]), -1)
				//Создаем новый файл с нужным нам путем с картинкой
				absPath, _ := filepath.Abs(resj.PwdHtml)
				fmt.Println(absPath)
				if err = ioutil.WriteFile(absPath+ /*"/"+*/ randStr(8)+".html", output, 0666); err != nil {
					fmt.Println(err)
					//os.Exit(1)
				}
			}
		}
		absPath, _ := filepath.Abs(resj.PwdImg)

		/* Что то делаем */

		//Удаляем файл с номером filelist[j]
		err := os.Remove(absPath + filelist[j])
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func check_loop() {
	for {
		chek_status()
		time.Sleep(10 * time.Second)
	}
}

// Функция генерации рамдомной строки
func randStr(strSize int) string {
	var dictionary string
	dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes)
}

//Функция запуска комманды в интерактивном режиме
func runCommand(cmdName string, arg ...string) error {
	cmd := exec.Command(cmdName, arg...)
	//cmd.Stdout = os.Stdout
	//cmd.Stderr = os.Stderr
	//cmd.Stdin = os.Stdin
	err := cmd.Run()
	return err
}
