/*

1. Структура каталогов равная этажам
----- Папки 1, 2, 3, 4, 5
2. Туда кидаем картинки. Например нам надо только на 2 и 3 этаж.
---- Демон проверяет папки на изменение. Если пустые, то ничего не делать
      - если обратное
	    Узнаем в какой папке лежат картинки, получаем список файлов.
	    Подключаем сетевой диск с этой папкой и копируем туда сгенерированные html и картинки
	    ---- Куда копировать узнаем из confog.conf
	    Удаляем файлы
	    Отключаем сетевой диск

3. Демон дальше весит и проверяет папки

*/

package main

import (
	//"bufio"

	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

type ConfPwd struct {
	Template string // Файл шаблона
	PwdHtml  string // Куда ложить готовые html
	PwdImg   string // Папка где лежать картинки
}

func main() {
	// Парсим json файл
	file_json, err := os.Open("conf.json")
	if err != nil {
		fmt.Printf("Не возможно найти файл настроки: %v\n", err)
		os.Exit(1)
	}
	decoderConf := json.NewDecoder(file_json)
	resj := ConfPwd{}
	err = decoderConf.Decode(&resj)
	if err != nil {
		fmt.Println("error:", err)
	}
	// Загружаем файл с шаблоном
	input, err := ioutil.ReadFile(resj.Template)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Создаем массив для записи в него имен картинок
	filelist := make(map[int]string)

	// Получаем список файлов в папке
	files, _ := ioutil.ReadDir(resj.PwdImg)
	//Пробегаемся по нему и пишим а filelist[]
	for j, f := range files {
		filelist[j] = f.Name()
		//Заменяем слово "zamena" на имя файла картинки
		output := bytes.Replace(input, []byte("zamena"), []byte(filelist[j]), -1)
		//Создаем новый файл с нужным нам путем с картинкой
		absPath, _ := filepath.Abs(resj.PwdHtml)
		fmt.Println(absPath)
		if err = ioutil.WriteFile(absPath+"/"+randStr(8)+".html", output, 0666); err != nil {
			fmt.Println(err)
			//os.Exit(1)
		}
	}
	if err := runCommand("uname", "-s"); err != nil {
		fmt.Println("Операционная система Windows")
		// Подключаем сетевой диск
		// Если диск не подключен, то подключаем
		if err := runCommand("net", "use", "x:", `\\saikov\ТМ Здоровье\Монитор вызова\img\img`, "/persistent:yes"); err != nil {
			// если жиск подключен, то отключаем его и цепляем заного
			runCommand("net", "use", "x:", "/delete")
			runCommand("net", "use", "x:", `\\saikov\ТМ Здоровье\Монитор вызова\img\img`, "/persistent:yes")
			fmt.Println("Hello")
		}

	}
	/*consolereader := bufio.NewReader(os.Stdin)
	fmt.Print("На какие этажи кидать картинки? ")
	input3, err := consolereader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(input3[1])) */

	fmt.Println("Супер! Все получилось!")
	fmt.Println("Исходный код на https://bitbucket.org/valek/monitor/src")
}

// Функция генерации рамдомной строки
func randStr(strSize int) string {
	var dictionary string
	dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes)
}

//Функция запуска комманды в интерактивном режиме
func runCommand(cmdName string, arg ...string) error {
	cmd := exec.Command(cmdName, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	return err
}

/*
  func main() {
          var response int
          fmt.Println("This program will activate SkyNet worldwide, are you sure about this?\n")

          fmt.Scanf("%c", &response) //<--- here
          switch response {
          default:
                  fmt.Println("SkyNet launch aborted!")
          case 'y':
                  fmt.Println("SkyNet launched")
          case 'Y':
                  fmt.Println("SkyNet launched")
          }
  }
*/
